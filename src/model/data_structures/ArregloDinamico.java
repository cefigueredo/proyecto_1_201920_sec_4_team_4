package model.data_structures;

import java.util.Iterator;
import java.util.Random;

import model.logic.*;

/**
 * 2019-01-23
 * Estructura de Datos Arreglo Dinamico de Strings.
 * El arreglo al llenarse (llegar a su maxima capacidad) debe aumentar su capacidad.
 * @author Fernando De la Rosa
 * @param <T>
 *
 */
public class ArregloDinamico<T> implements IArregloDinamico<T> {
		/**
		 * Capacidad maxima del arreglo
		 */
        private int tamanoMax;
		/**
		 * Numero de elementos presentes en el arreglo (de forma compacta desde la posicion 0)
		 */
        private int tamanoAct;
        /**
         * Arreglo de elementos de tamaNo maximo
         */
        private T[] elementos;

        /**
         * Apuntador a la cabeza de la estructura de datos (cola)
         */
     
        /**
         * Apuntador a la cola de la estructura de datos (cola).
         */
       
        /**
         * Construir un arreglo con la capacidad maxima inicial.
         * @param max Capacidad maxima inicial
         */
		@SuppressWarnings("unchecked")
		public ArregloDinamico( int max )
        {
               elementos = (T[]) new UBERTrip[max];
              
               tamanoMax = max;
               tamanoAct = 0;
        }
        @SuppressWarnings("unchecked")
		public T agregar( T dato )
        {

               if ( tamanoAct == tamanoMax )
               {  // caso de arreglo lleno (aumentar tamaNo)
                    tamanoMax = 2 * tamanoMax;
                    T [ ] copia = elementos;
                    elementos = (T[]) new UBERTrip[tamanoMax];
                    for ( int i = 0; i < tamanoAct; i++)
                    {
                     	 elementos[i] = copia[i];
                    } 
            	    System.out.println("Arreglo lleno: " + tamanoAct + " - Arreglo duplicado: " + tamanoMax);
               }
               if(tamanoAct == 0) {
            	   
            	   elementos[tamanoAct] =  dato;
                   tamanoAct++;
                   
               }
               else {
                   elementos[tamanoAct] = dato;
                   tamanoAct++;
                   
               }
               return dato;
       }

		public int darCapacidad() {
			return tamanoMax;
		}

		public int darTamano() {
			return tamanoAct;
		}

		public T darElemento(int i) {
			
			return elementos[i];
		}


		public T eliminar() {
			T rta = null;
			
			
			return rta;

		}
		
		public void exch(int pos1, int pos2)
		{
			T aux =elementos[pos1];
			elementos[pos1]=elementos[pos2];
			elementos[pos2]=aux;
		}

		/**
		 * Agrega elemento en posici�n dada.
		 */
		public void posicionarDato(UBERTrip uberTrip, int pos) {
			if(pos <= tamanoAct) {
				elementos[pos] = (T) uberTrip;
			}
		}

		public void shuffleArray(ArregloDinamico<? extends UBERTrip> pViajes)
		{
		    UBERTrip temp;
		    int index;
		    Random random = new Random();
		    for (int i = pViajes.tamanoAct - 1; i > 0; i--)
		    {
		        index = random.nextInt(i + 1);
		        temp = pViajes.darElemento(index);
		        pViajes.posicionarDato(pViajes.darElemento(i), index); 
		        pViajes.posicionarDato(temp, i);;
		    }
		}
}
