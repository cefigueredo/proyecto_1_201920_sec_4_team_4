package model.logic;

public class UBERTripHour extends UBERTrip {

	private double hod;
	public UBERTripHour(double pSourceid, double pDstid, double pHod, double pMeanTravelTime, double pStandardDeviationTravelTime,
			double pGeometricMeanTravelTime, double pGeometricStandardDeviationTravelTime) {
		super(pSourceid, pDstid, pMeanTravelTime, pStandardDeviationTravelTime, pGeometricMeanTravelTime,
				pGeometricStandardDeviationTravelTime);
		hod = pHod;
	}
	public double getHod() {
		return hod;
	}

}
