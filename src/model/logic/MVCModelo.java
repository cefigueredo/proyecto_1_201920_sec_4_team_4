package model.logic;

import java.io.FileReader;

import com.opencsv.CSVReader;

import model.data_structures.*;
/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo<T> {
	/**
	 * Atributos del modelo del mundo
	 */
	private ArregloDinamico<UBERTripMonth> dataMonth;
	private ArregloDinamico<UBERTripDay> dataDow;
	private ArregloDinamico<UBERTripHour> dataHod;
	private int trimestreAnio;
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		dataMonth = new ArregloDinamico<UBERTripMonth>(1);
		dataDow = new ArregloDinamico<UBERTripDay>(1);
		dataHod = new ArregloDinamico<UBERTripHour>(1);

	}
	
	public MVCModelo(int capacidad) {
		// TODO Auto-generated constructor stub
		dataMonth = new ArregloDinamico<UBERTripMonth>(capacidad);
		dataDow = new ArregloDinamico<UBERTripDay>(capacidad);
		dataHod = new ArregloDinamico<UBERTripHour>(capacidad);

	}

	@SuppressWarnings("unchecked")
	public String cargarDatos(int numeroTrimestre) throws Exception {
		trimestreAnio = numeroTrimestre;
		String[] lineMonthly = null;
		String[] lineWeekly = null;
		String[] lineHourly = null;

		CSVReader readerMonth = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+numeroTrimestre+"-All-MonthlyAggregate.csv"));
		CSVReader readerWeek = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+numeroTrimestre+"-WeeklyAggregate.csv"));
		CSVReader readerDay = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+numeroTrimestre+"-All-HourlyAggregate.csv"));
	
		readerMonth.readNext();
		readerWeek.readNext();
		readerDay.readNext();

		int mTrip = 0;
		while((lineMonthly = readerMonth.readNext()) != null ) {
			UBERTripMonth dato = new UBERTripMonth(Double.parseDouble(lineMonthly[0]), Double.parseDouble(lineMonthly[1]), Double.parseDouble(lineMonthly[2]), Double.parseDouble(lineMonthly[3]), Double.parseDouble(lineMonthly[4]), Double.parseDouble(lineMonthly[5]), Double.parseDouble(lineMonthly[6]));
			dataMonth.agregar(dato);
			mTrip++;
		}
		int dTrip = 0;
		while((lineWeekly = readerWeek.readNext()) != null ) {
			
			UBERTripDay dato = new UBERTripDay(Double.parseDouble(lineWeekly[0]), Double.parseDouble(lineWeekly[1]), Double.parseDouble(lineWeekly[2]), Double.parseDouble(lineWeekly[3]), Double.parseDouble(lineWeekly[4]), Double.parseDouble(lineWeekly[5]), Double.parseDouble(lineWeekly[6]));
			dataDow.agregar(dato);
			dTrip++;
		}
		int hTrip = 0;
		while((lineHourly = readerDay.readNext()) != null ) {
			
			UBERTripHour dato = new UBERTripHour(Double.parseDouble(lineHourly[0]), Double.parseDouble(lineHourly[1]), Double.parseDouble(lineHourly[2]), Double.parseDouble(lineHourly[3]), Double.parseDouble(lineHourly[4]), Double.parseDouble(lineHourly[5]), Double.parseDouble(lineHourly[6]));
			dataHod.agregar(dato);
			hTrip++;
		}
		double minZone = 99999;
		double maxZone = 0;
		for( int i = 0; i < dataMonth.darTamano(); i++) {
			if(dataMonth.darElemento(i).getDstid() < minZone) {
				minZone = dataMonth.darElemento(i).getDstid();
			}
			if(dataMonth.darElemento(i).getSourceid() < minZone) {
				minZone = dataMonth.darElemento(i).getSourceid();
			}
			if(dataMonth.darElemento(i).getDstid() > maxZone) {
				maxZone = dataMonth.darElemento(i).getDstid();
			}
			if(dataMonth.darElemento(i).getSourceid() > maxZone) {
				maxZone = dataMonth.darElemento(i).getSourceid();
			}
		}
		
		for( int i = 0; i < dataDow.darTamano(); i++) {
			if(dataDow.darElemento(i).getDstid() < minZone) {
				minZone = dataDow.darElemento(i).getDstid();
			}
			if(dataDow.darElemento(i).getSourceid() < minZone) {
				minZone = dataDow.darElemento(i).getSourceid();
			}
			if(dataDow.darElemento(i).getDstid() > maxZone) {
				maxZone = dataDow.darElemento(i).getDstid();
			}
			if(dataDow.darElemento(i).getSourceid() > maxZone) {
				maxZone = dataDow.darElemento(i).getSourceid();
			}
		}
		
		for( int i = 0; i < dataHod.darTamano(); i++) {
			if(dataHod.darElemento(i).getDstid() < minZone) {
				minZone = dataHod.darElemento(i).getDstid();
			}
			if(dataHod.darElemento(i).getSourceid() < minZone) {
				minZone = dataHod.darElemento(i).getSourceid();
			}
			if(dataHod.darElemento(i).getDstid() > maxZone) {
				maxZone = dataHod.darElemento(i).getDstid();
			}
			if(dataHod.darElemento(i).getSourceid() > maxZone) {
				maxZone = dataHod.darElemento(i).getSourceid();
			}
		}
		
		String reporte = "Total viajes en el archivo de meses: "+mTrip+"\nTotal viajes en el archivo de d�as: "+dTrip+"\nTotal viajes en el archivo de horas: "+hTrip+"\nZona con menor id identificado: "+minZone+"\nZona con mayor id identificado: "+maxZone+"\n";
		return reporte;
	}
	public String darTiempoPromedioPorMes(int mesDado, double origen, double destino) {
		String r = "Viajes encontrados para el mes dado entre las zonas dadas: \n";
		int nViajes = 0;
		for(int i = 0; i < dataMonth.darTamano(); i++) {
			if(dataMonth.darElemento(i).getMonth() == mesDado && dataMonth.darElemento(i).getSourceid() == origen && dataMonth.darElemento(i).getDstid() == destino) {
				nViajes++;
				r+="Tiempo promedio: "+dataMonth.darElemento(i).getMeanTravelTime()+" Desviacion estandar: "+dataMonth.darElemento(i).getStandardDeviationTravelTime()+"\n";
			}
		}
		if(nViajes == 0) {
			return "No se encontraron viajes para el mes dado, zona de origen y destino dados.\n";
		}
		return r;
	}
	@SuppressWarnings("unchecked")
	public String darNViajesMayorTiempoPromedioPorMes(int mesDado, int nViajes) throws Exception {
		String r = "";
		//quickSortStart(dataMonth);
		r+="Viajes ordenados de mayor a menor por tiempo promedio: \n";
		int i = 0;
		int j = dataMonth.darTamano()-1;

		while(i < nViajes && j > 0) {
			if(dataMonth.darElemento(j).getMonth() == mesDado) {
				r += "Zona origen: "+dataMonth.darElemento(j).getSourceid()+" Zona destino: "+dataMonth.darElemento(j).getDstid()
						+ " Tiempo promedio: "+dataMonth.darElemento(j).getMeanTravelTime()+" Desviacion estandar: "+dataMonth.darElemento(j).getStandardDeviationTravelTime()+"\n";
				i++;
			}
			j--;
		}
		
		return r;
	}
	
	public String comparacionTiempoPromedioEntreZonasPorMes(int mesDado, double zonaA, double zonaB) {
		String r = "";
		double tiempoAB = 0;
		int viajesAB = 0;
		double tiempoBA = 0;
		int viajesBA = 0;
		String noAB = "No hay viajes";
		String noBA = "No hay viajes";
		for(int i = 0; i < dataMonth.darTamano(); i++) {
			if(dataMonth.darElemento(i).getMonth() == mesDado && dataMonth.darElemento(i).getSourceid() == zonaA && dataMonth.darElemento(i).getDstid() == zonaB) {
				tiempoAB += dataMonth.darElemento(i).getMeanTravelTime();
				viajesAB++;
			}
			else if(dataMonth.darElemento(i).getMonth() == mesDado && dataMonth.darElemento(i).getSourceid() == zonaB && dataMonth.darElemento(i).getDstid() == zonaA) {
				tiempoBA += dataMonth.darElemento(i).getMeanTravelTime();
				viajesBA++;
			}
		}
		if(viajesAB > 0) {
			noAB = (tiempoAB/viajesAB) + " ";
		}
		if(viajesBA > 0) {
			noBA = (tiempoBA/viajesBA) + " ";
		}
		r = noAB + " de "+zonaA+" a "+zonaB+" vs "+noBA+" de "+zonaB+" a "+zonaA + "\n";
		return r;
	}

	public String consultarEntreZonasYHoras(double zonaA, double zonaB, int horaA, int horaB) {
		String r = "";
		int i = 0;
		int hora = horaA;
		while(hora <= horaB ) {
			r+="Viajes para la hora "+hora+" \n";
			while( i < dataHod.darTamano()) {
				if(dataHod.darElemento(i).getHod() == hora && dataHod.darElemento(i).getSourceid() == zonaA && dataHod.darElemento(i).getDstid() == zonaB) {
					r+= "Tiempo promedio: "+dataHod.darElemento(i).getMeanTravelTime()+" Desviacion: "+dataHod.darElemento(i).getStandardDeviationTravelTime() + "\n";
				}
				i++;
			}
			hora++;
			i = 0;
		}
		return r;
	}
	public String consultarNViajesMayorPromedioHoraDada(int horaDada, int nViajes) throws Exception {
		String r = "";
		int i = 0; 
		int j = dataHod.darTamano() - 1;
		sortHour(dataHod);
		while(i < nViajes && j > 0) {
			if(dataHod.darElemento(j).getHod() == horaDada) {
				r = r+(i+1)+" Zona origen: "+dataHod.darElemento(j).getSourceid()+" Zona destino: "+dataHod.darElemento(j).getDstid()+" Tiempo promedio: "+dataHod.darElemento(j).getMeanTravelTime()+" Desviaci�n: "+dataHod.darElemento(j).getStandardDeviationTravelTime()+"\n";
				i++;
			}		
			j--;
		}
		return r;
	}
	
	public String graficarTiempoViajesEntreZonasCadaHora(double zonaA, double zonaB) {
		String r = "Aproximaci�n en minutos de viajes entre zona origen y zona destino.\n"
				+ "Trimestre "+trimestreAnio+" del 2018 detallado por cada hora del dia. \n"
				+" Zona origen: "+zonaA+"\nZona destino: "+zonaB+"\nHora|  # de minutos \n";
		int hora = 0;
		int i = 0;
		while(hora <= 23 ) {
			String padding = String.format("%02d" , hora);
			r+= padding+"  |  ";
			boolean existe = false;
			while( i < dataHod.darTamano()) {
				if(dataHod.darElemento(i).getHod() == hora && dataHod.darElemento(i).getSourceid() == zonaA && dataHod.darElemento(i).getDstid() == zonaB) {
					double min = dataHod.darElemento(i).getMeanTravelTime()/60;
					for(int j = 0; j < Math.round(min); j++) {
						r+= "*";
					}
					r+="\n";
					existe = true;
				}
				i++;
			}
			if(!existe) {
				r+="hora sin viajes\n";
			}
			hora++;
			i = 0;
		}
		return r;
	}
	
	
	public int darTamano() {
		// TODO Auto-generated method stub
		return (dataMonth.darTamano()+dataDow.darTamano()+dataHod.darTamano());
		
	}
	
	

	private static int partition(ArregloDinamico<UBERTrip> a, int lo, int hi)
	{ // Partition into a[lo..i-1], a[i], a[i+1..hi].
	int i = lo, j = hi+1; // left and right scan indices
	UBERTrip v = a.darElemento(lo); // partitioning item
	while (true)
	{ // Scan right, scan left, check for scan complete, and exchange.
	while (less(a.darElemento(++i), v)) if (i == hi) break;
	while (less(v, a.darElemento(--j))) if (j == lo) break;
	if (i >= j) break;
	exch(a, i, j);
	}
	exch(a, lo, j); // Put v = a[j] into position
	return j; // with a[lo..j-1] <= a[j] <= a[j+1..hi].
	}
	private static void exch(ArregloDinamico<UBERTrip> a, int i, int j) {
		// TODO Auto-generated method stub
		a.exch(i, j);
	}

	private static boolean less(UBERTrip v, UBERTrip darElemento) {
		// TODO Auto-generated method stub
		return v.compareTo(darElemento)<0;
	}

	private static void sort(ArregloDinamico<UBERTrip> a)
	{
		a.shuffleArray(a); // Eliminate dependence on input.
		sort(a, 0, a.darTamano() - 1);

	}
	private static void sort(ArregloDinamico<UBERTrip> a, int lo, int hi) {
		if (hi <= lo) return;
		int j = partition(a, lo, hi);
		sort(a, lo, j-1); // Sort left part a[lo .. j-1].
		sort(a, j+1, hi); // Sort right part a[j+1 .. hi].
		}

	private static int partitionDay(ArregloDinamico<UBERTripDay> a, int lo, int hi)
	{ // Partition into a[lo..i-1], a[i], a[i+1..hi].
	int i = lo, j = hi+1; // left and right scan indices
	UBERTrip v = a.darElemento(lo); // partitioning item
	while (true)
	{ // Scan right, scan left, check for scan complete, and exchange.
	while (less(a.darElemento(++i), v)) if (i == hi) break;
	while (less(v, a.darElemento(--j))) if (j == lo) break;
	if (i >= j) break;
	exchDay(a, i, j);
	}
	exchDay(a, lo, j); // Put v = a[j] into position
	return j; // with a[lo..j-1] <= a[j] <= a[j+1..hi].
	}
	private static void exchDay(ArregloDinamico<UBERTripDay> a, int i, int j) {
		// TODO Auto-generated method stub
		a.exch(i, j);
	}

	

	private static void sortDay(ArregloDinamico<UBERTripDay> a)
	{
		a.shuffleArray(a); // Eliminate dependence on input.
		sortDay(a, 0, a.darTamano() - 1);

	}
	private static void sortDay(ArregloDinamico<UBERTripDay> a, int lo, int hi) {
		if (hi <= lo) return;
		int j = partitionDay(a, lo, hi);
		sortDay(a, lo, j-1); // Sort left part a[lo .. j-1].
		sortDay(a, j+1, hi); // Sort right part a[j+1 .. hi].
		}
	

	private static int partitionHour(ArregloDinamico<UBERTripHour> a, int lo, int hi)
	{ // Partition into a[lo..i-1], a[i], a[i+1..hi].
	int i = lo, j = hi+1; // left and right scan indices
	UBERTrip v = a.darElemento(lo); // partitioning item
	while (true)
	{ // Scan right, scan left, check for scan complete, and exchange.
	while (less(a.darElemento(++i), v)) if (i == hi) break;
	while (less(v, a.darElemento(--j))) if (j == lo) break;
	if (i >= j) break;
	exchHour(a, i, j);
	}
	exchHour(a, lo, j); // Put v = a[j] into position
	return j; // with a[lo..j-1] <= a[j] <= a[j+1..hi].
	}
	private static void sortHour(ArregloDinamico<UBERTripHour> a)
	{
		a.shuffleArray(a); // Eliminate dependence on input.
		sortHour(a, 0, a.darTamano() - 1);

	}
	private static void sortHour(ArregloDinamico<UBERTripHour> a, int lo, int hi) {
		if (hi <= lo) return;
		int j = partitionHour(a, lo, hi);
		sortHour(a, lo, j-1); // Sort left part a[lo .. j-1].
		sortHour(a, j+1, hi); // Sort right part a[j+1 .. hi].
		}


	private static void exchHour(ArregloDinamico<UBERTripHour> a, int i, int j) {
		// TODO Auto-generated method stub
		a.exch(i, j);
	}

	public String tiempoPromedioEntreZonasDia(int zonaA, int zonaB, int dia) {
		double sumaT = 0;
		int cont = 0;
		double sumaD = 0;
		for(int i=0;i<dataDow.darTamano();i++)
		{
			if(dataDow.darElemento(i).getSourceid()==zonaA && dataDow.darElemento(i).getDstid()==zonaB&& dataDow.darElemento(i).getDow()==dia)
			{
				sumaT+=dataDow.darElemento(i).getMeanTravelTime();
				
				cont++;
			}
		}
		if(cont==0) return "No se encontraron viajes para el d�a dado entre las zonas dadas.";
		sumaT/=cont;
		
		for(int i=0;i<dataDow.darTamano();i++)
		{
			if(dataDow.darElemento(i).getSourceid()==zonaA && dataDow.darElemento(i).getDstid()==zonaB&& dataDow.darElemento(i).getDow()==dia)
			{
				sumaD+=(dataDow.darElemento(i).getMeanTravelTime()-sumaT)*(dataDow.darElemento(i).getMeanTravelTime()-sumaT);
			}
		}
		sumaD/=cont;
		sumaD=Math.sqrt(sumaD);
		String rta="El tiempo promedio de viaje es: "+sumaT+"\nLa desviaci�n est�ndar de los viajes es: "+sumaD;
		return rta;
	}
	public String mayorTiempoPromedioDia(int pNumero, int pDia)
	{
		int j=0;
		String rta="Los "+pNumero+" viajes mayores son: \n";
		try {
			sortDay(dataDow);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(int i=dataDow.darTamano()-1;i>=0 && j<10;i--)
		{
			if(dataDow.darElemento(i).getDow()==pDia)
			{
				j++;
				rta=rta+"\n"+j+" Zona de origen: "+dataDow.darElemento(i).getSourceid()
						+"\t Zona destino: "+dataDow.darElemento(i).getDstid()
						+"\t Tiempo promedio de viaje: "+dataDow.darElemento(i).getMeanTravelTime()
						+"\t Desviaci�n est�ndar: "+dataDow.darElemento(i).getStandardDeviationTravelTime();
				
			}
		}
		return rta;
	}
	public String comparacionTiempoPromedioZonasDias(int Zona1, int ZonaRangoMay,int ZonaRangoMen, int DoW)
	{
		String rta="";
		double tAB=0;
		double tBA=0;
		int cont=0;
		for(int i=ZonaRangoMen; i<=ZonaRangoMay;i++)
		{
			for(int j=0;j<dataDow.darTamano();j++)
			{
				if(dataDow.darElemento(j).getSourceid()==Zona1
						&& dataDow.darElemento(j).getDstid()==i
						&& dataDow.darElemento(j).getDow()==DoW)
				{
					tAB+=dataDow.darElemento(j).getMeanTravelTime();
					cont++;
				}
			}
			if(tAB==0)rta+="\n No hay viajes"+" de Zona "+Zona1+" a zona "+i;
			else{
				
				tAB=tAB/cont;
				rta+="\n "+tAB+ " de Zona "+Zona1+" a zona "+i;
			}
			cont=0;
			for(int h=0; h<dataDow.darTamano();h++)
			{
				if(dataDow.darElemento(h).getSourceid()==i
						&& dataDow.darElemento(h).getDstid()==Zona1
						&& dataDow.darElemento(h).getDow()==DoW)
				{
					tBA+=dataDow.darElemento(h).getMeanTravelTime();
					cont++;
				}
			}
			if(tBA==0)rta+="\t vs \t No hay viajes"+" de Zona "+i+" a zona "+Zona1;
			else 
				{
				
				tBA=tBA/cont;
				rta+="\t vs \t "+ tBA+" de Zona "+i+" a zona "+Zona1;
				}
			cont=0;
		}
		return rta;
	}

	
}
