package controller;

import java.util.Scanner;

import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() throws Exception 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		
		while( !fin ){
			view.printMenu();
			int option = lector.nextInt();
			switch(option){
				case 1:
					System.out.println("\n Cargar datos\nDar numero del trimestre:\n");
					int trimestre = lector.nextInt();
					String r = modelo.cargarDatos(trimestre);
					System.out.println(r);
					break;
				case 2:
					System.out.println("Para calcular ingrese lo siguiente:\nMes dado: ");
					int mes = lector.nextInt();
					System.out.println("Zona or�gen: ");
					int origen = lector.nextInt();
					System.out.println("Zona destino: ");
					int destino = lector.nextInt();
					String r2 = modelo.darTiempoPromedioPorMes(mes, origen, destino);
					System.out.println(r2);
					break;
				case 3:
					System.out.println("Para calcular N viajes ingrese: \n Mes: ");
					int mes1 = lector.nextInt();
					System.out.println("N viajes: ");
					int n = lector.nextInt();
					String r3 = modelo.darNViajesMayorTiempoPromedioPorMes(mes1, n);
					System.out.println(r3);
					break;
				case 4:
					System.out.println("Para calcular tiempo promedio entre zonas por mes, ingrese: \nMes:");
					int mesZ = lector.nextInt();
					System.out.println("Zona inicio:");
					int zonaF = lector.nextInt();
					System.out.println("Zona final: ");
					int zonaG = lector.nextInt();
					String rg = modelo.comparacionTiempoPromedioEntreZonasPorMes(mesZ, zonaF, zonaG);
					System.out.println(rg);
					break;
				case 5: 
					System.out.println("Para calcular tiempo promedio entre zonas por dia ingrese:\n Dia: ");
					int dia = lector.nextInt();
					System.out.println("Zona inicio: ");
					int zonaA = lector.nextInt();
					System.out.println("Zona final: ");
					int zonaB = lector.nextInt();
					String r5= modelo.tiempoPromedioEntreZonasDia(zonaA, zonaB, dia);
					System.out.println(r5);
					break;
				case 6:
					System.out.println("Para calcular N viajes ingrese: \n Dia: ");
					int pDia = lector.nextInt();
					System.out.println("N viajes: ");
					int pNumero = lector.nextInt();
					String r6 = modelo.mayorTiempoPromedioDia(pNumero, pDia);
					System.out.println(r6);
					break;
				case 7:
					System.out.println("Para calcular tiempo promedio entre zonas por dia, ingrese: \nDia:");
					int DoW = lector.nextInt();
					System.out.println("Zona de referencia: ");
					int Zona1=lector.nextInt();
					System.out.println("Zona inicio:");
					int ZonaRangoMen = lector.nextInt();
					System.out.println("Zona final: ");
					int ZonaRangoMay = lector.nextInt();
					String r7 = modelo.comparacionTiempoPromedioZonasDias(Zona1, ZonaRangoMay, ZonaRangoMen, DoW);
					System.out.println(r7);
					break;
				case 8:
					System.out.println("Para calcular, ingrese: \n Zona origen:");
					double zona1 = lector.nextDouble();
					System.out.println("Zona destino: ");
					double zona2 = lector.nextDouble();
					System.out.println("Hora inicial: ");
					int horaI = lector.nextInt();
					System.out.println("Hora final: ");
					int horaF = lector.nextInt();
					String rr = modelo.consultarEntreZonasYHoras(zona1, zona2, horaI, horaF);
					System.out.println(rr);
					break;
				case 9:
					System.out.println("Para calcular, ingrese: \n Hora: ");
					int horaM = lector.nextInt();
					System.out.println("Cantidad de viajes: ");
					int viajesM = lector.nextInt();
					String rm = modelo.consultarNViajesMayorPromedioHoraDada(horaM, viajesM);
					System.out.println(rm);
					break;
				case 10:
					System.out.println("Para graficar, ingrese: \n Zona origen: ");
					double zonaX = lector.nextDouble();
					System.out.println("Zona destino: ");
					double zonaY = lector.nextDouble();
					String rx = modelo.graficarTiempoViajesEntreZonasCadaHora(zonaX, zonaY);
					System.out.println(rx);
					break;
				case 11: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;	

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
