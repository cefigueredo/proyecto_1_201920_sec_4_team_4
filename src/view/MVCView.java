package view;

import model.logic.MVCModelo;

public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    }
	    
		public void printMenu()
		{
			System.out.println("1. Cargar datos");
			System.out.println("2. Consultar tiempo promedio de viaje entre zonas en un mes dado.");
			System.out.println("3. Consultar N mayores viajes en un mes dado");
			System.out.println("4. Consultar tiempo promedio entre zonas por mes");
			System.out.println("5. Consultar tiempo promedio de viaje entre zonas en un dia dado.");
			System.out.println("6. Consultar N mayores viajes en un d�a dado");
			System.out.println("7. Consultar tiempo promedio entre zonas por dia");
			System.out.println("8. Consultar viajes entre zonas y horas");
			System.out.println("9. Consultar N viajes para hora dada");
			System.out.println("10. Grafica tiempo promedio entre zonas para cada hora del dia");
			System.out.println("11. Exit");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			// TODO implementar
		}
}
